import express from 'express'
import cors from 'cors'
import PaymentController from './controllers/PaymentController'

const knex = require('knex')({
    client: 'mysql',
    connection: process.env.CLEARDB_DATABASE_URL
});

async function main() {
  const app = express()
  const bodyParser = require('body-parser')
  app.use(cors())

  app.use(bodyParser.json()) // support json encoded bodies
  app.use(bodyParser.urlencoded({
    extended: true
  })) // support encoded bodies

  const exists = await knex.schema.hasTable('payment');

  if (!exists) {
      await knex.schema.createTable('payment', (t) => {
          t.increments('id');
          t.integer('sale_id');
          t.integer('product_id');
          t.integer('buyer_id');
          t.integer('seller_id');
          t.float('price');
          t.integer('payment_type');
      });
  }

  app.get('/favicon.ico', function (req, res) {
    res.send()
  })

  app.get('/', async (req, res) => res.send(await PaymentController.hello(req, res)))

  app.post('/pay', async (req, res) => res.send(await PaymentController.pay(req, res)))

  // app.post('/search', async (req, res) => {
  //   res.setHeader('Content-Type', 'application/json')
  //   try {
  //     const result = await PaymentController.search(req, res)
  //     res.send(`${JSON.stringify(result)}`)
  //   } catch (err) {
  //     res.status(err.status || 400).send(err.error || '')
  //   }
  // })

  function clientErrorHandler (err, req, res, next) {
    if (req.xhr) {
      res.status(500).send({ error: 'Something failed!' })
    } else {
      next(err)
    }
  }
  app.use(clientErrorHandler)

  const port = process.env.PORT || 3000
  const hostname = process.env.HOSTNAME || null
  const server = app.listen(port, hostname, () => {
    const host = server.address().address
    const port = server.address().port
    console.log(`payment-center Listening on ${host}:${port}`)
  })
  // Notes: 
  // res.writeHead(200, {'Content-Type': 'text/html'})
  // res.end(body)
  // return

}
main()