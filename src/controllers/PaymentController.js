const axios = require('axios')
const knex = require('knex')({
    client: 'mysql',
    connection: process.env.CLEARDB_DATABASE_URL
});


class PaymentController {

  async hello (req, res) {
    return 'Hello from payment-center'
  } 


  async pay (req, res) {
    try {
        res.setHeader('Content-Type', 'application/json')
        const result = await knex('payment').insert({
            sale_id: req.body['sale_id'],
            product_id: req.body['product_id'],
            buyer_id: req.body['buyer_id'],
            seller_id: req.body['seller_id'],
            price: req.body['price'],
            payment_type: req.body['payment_type']
        });
        const payment_id = result[0]
        const hookResult = await axios.post(`${process.env.APP_FINANCE}/payment-confirmation-webhook`,{
            sale_id: req.body['sale_id']
        })
        console.log("💰 Compra recebida id #" + req.body['sale_id'] + " ✅")
        return true
    } catch (err) {
        console.error(err);
        return res.status(500).send('Something went wrong. Try again later');
    }
  }
}

module.exports = new PaymentController()